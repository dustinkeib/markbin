import { Meteor } from 'meteor/meteor';
import { Bins } from '../imports/collections/bins'

// code to run on server at startup
Meteor.startup(() => {
  // Reg function to access this.userId
  // returns only bins which match this.userId
  Meteor.publish('bins', function() {
    return Bins.find({ ownerId: this.userId })
  })

  Meteor.publish('sharedBins', function() {

    // get this user
    const user = Meteor.users.findOne(this.userId)

    // no user logged in
    if (!user) { return }

    // only 1 email address in app
    const email = user.emails[0].address

    // for all bins, find sharedWith that an element is equal to email
    return Bins.find({ 
      sharedWith: { $elemMatch: { $eq: email} }
    })
  })

});
